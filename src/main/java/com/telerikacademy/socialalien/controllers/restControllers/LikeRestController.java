package com.telerikacademy.socialalien.controllers.restControllers;

import com.telerikacademy.socialalien.exceptions.EntityNotFoundException;
import com.telerikacademy.socialalien.models.Like;
import com.telerikacademy.socialalien.models.Post;
import com.telerikacademy.socialalien.models.User;
import com.telerikacademy.socialalien.services.contracts.LikeService;
import com.telerikacademy.socialalien.services.contracts.PostService;
import com.telerikacademy.socialalien.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.PublicKey;
import java.util.List;

@RestController
@RequestMapping("/api/likes")
public class LikeRestController {

    private LikeService likeService;
    private UserService userService;
    private PostService postService;

    @Autowired
    public LikeRestController(LikeService likeService, UserService userService, PostService postService) {
        this.likeService = likeService;
        this.userService = userService;
        this.postService=postService;
    }

    @GetMapping
    public List<Like> getAll() {

        return likeService.getAll();
    }

    @GetMapping("/{id}")
    public Like getById(@Valid @PathVariable int id) {
        return likeService.findById(id);
    }

    @PostMapping("/create")
    public Like createComment(@RequestBody Like like) {
        likeService.create(like);
        return likeService.findById(like.getId());
    }

    @PutMapping
    public Like update(@RequestBody @Valid Like like) {
        return likeService.update(like);
    }

    @DeleteMapping
    public Like delete(@RequestBody @Valid Like like) {
        return likeService.delete(like);
    }

    @GetMapping("/findByPost")
    public List<Like> findByPost(@RequestBody @Valid Post post) {
        if (postService.findById(post.getId()) == null)
            throw new EntityNotFoundException("There is no such post with id: " + post.getId());
        return likeService.findAllByPost(post);
    }
    @GetMapping("/findByUser")
    public List<Like> findByUser(@RequestBody @Valid User user) {
        if (!userService.getUserById(user.getId()).isPresent())
            throw new EntityNotFoundException("There is no such user with id: " + user.getId());
        return likeService.findAllByUser(user);
    }

    @PostMapping("/createLikeOnPost")
    public Like createLikeOnPost(@RequestParam String userName, @RequestParam int postId) {

        if (!userService.getUserByUsername(userName).isPresent())
            throw new EntityNotFoundException("There is no user with username:" + userName);
        if (likeService.findById(postId) == null)
            throw new EntityNotFoundException("There is no such post with id: " + postId);
         return likeService.createLikeOnPost(userName,postId);

    }
}
