package com.telerikacademy.socialalien.controllers.restControllers;


import com.telerikacademy.socialalien.exceptions.EntityNotFoundException;
import com.telerikacademy.socialalien.models.Comment;
import com.telerikacademy.socialalien.models.Post;
import com.telerikacademy.socialalien.models.User;
import com.telerikacademy.socialalien.services.contracts.CommentService;
import com.telerikacademy.socialalien.services.contracts.PostService;
import com.telerikacademy.socialalien.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/comments")
public class CommentRestController {

    private CommentService commentService;
    private UserService userService;
    private PostService postService;

    @Autowired
    public CommentRestController(CommentService commentService, UserService userService, PostService postService) {
        this.commentService = commentService;
        this.userService = userService;
        this.postService = postService;
    }

    @GetMapping
    public List<Comment> getAll() {
        return commentService.getAll();
    }

    @GetMapping("/{id}")
    public Comment getById(@Valid @PathVariable int id) {
        return commentService.findById(id);
    }

    @PostMapping("/create")
    public Comment createComment(@Valid @RequestBody Comment comment) {
        commentService.create(comment);
        return commentService.findById(comment.getId());
    }
    @PutMapping
    public Comment update(@RequestBody Comment comment) {
        commentService.update(comment);
        return commentService.findById(comment.getId());
    }
    @DeleteMapping
    public Comment delete(@RequestBody Comment comment) {
        commentService.delete(comment);
        return commentService.findById(comment.getId());
    }

    @GetMapping("/findByPost")
    public List<Comment> findByPost(@RequestBody @Valid Post post) {
        if (postService.findById(post.getId()) == null)
            throw new EntityNotFoundException("There is no such post with id: " + post.getId());
        return commentService.findAllByPost(post);
    }

    @GetMapping("/findByUser")
    public List<Comment> findByUser(@RequestBody @Valid User user) {
        if (!userService.getUserById(user.getId()).isPresent())
            throw new EntityNotFoundException("There is no such user with id: " + user.getId());
        return commentService.getAllByUser(user);
    }
}
