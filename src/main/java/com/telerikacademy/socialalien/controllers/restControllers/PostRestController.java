package com.telerikacademy.socialalien.controllers.restControllers;

import com.telerikacademy.socialalien.exceptions.EntityNotFoundException;
import com.telerikacademy.socialalien.models.Post;
import com.telerikacademy.socialalien.models.User;
import com.telerikacademy.socialalien.models.enums.PrivacyType;
import com.telerikacademy.socialalien.services.contracts.PostService;
import com.telerikacademy.socialalien.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
@RestController
@RequestMapping("/api/posts")
public class PostRestController {

    private PostService postService;
    private UserService userService;

    public PostRestController(PostService postService, UserService userService) {
        this.postService = postService;
        this.userService = userService;
    }

    @Autowired

    @GetMapping
    public List<Post> getAll() {

        return postService.getAll();
    }

    @GetMapping("/{id}")
    public Post getById(@Valid @PathVariable int id) {
        return postService.findById(id);
    }

    @PostMapping("/create")
    public Post create(@Valid @RequestBody Post post) {
        postService.create(post);
        return postService.findById(post.getId());
    }

    @PutMapping
    public Post update(@RequestBody Post post) {
        postService.update(post);
        return postService.findById(post.getId());
    }

    @DeleteMapping
    public Post delete(@RequestBody Post post) {
        postService.delete(post);
        return postService.findById(post.getId());
    }

    @GetMapping("/createNewsFeed")
    public List<Post>createNewsFeed(@RequestParam String principalUserName){
        if(!userService.getUserByUsername(principalUserName).isPresent())
            throw new EntityNotFoundException("There is no user with username:" + principalUserName);
       return  postService.createNewsFeed(principalUserName);
    }
    @GetMapping("/createUserFeed")
    public List<Post>createUserFeed(@RequestParam String principalUserName, @RequestParam int userId){
        if(!userService.getUserByUsername(principalUserName).isPresent())
            throw new EntityNotFoundException("There is no user with username:" + principalUserName);
        if(!userService.getUserById(userId).isPresent())
            throw new EntityNotFoundException("There is no user with id:" + userId);
        return  postService.createUserFeed(principalUserName, userId);
    }
    @GetMapping("/getByUserId")
    public List<Post> getByUserId(@RequestParam int userId)
    {
        if(!userService.getUserById(userId).isPresent())
            throw new EntityNotFoundException("There is no user with id:" + userId);
       return postService.findAllByUserId(userId);
    }

    @GetMapping("/getByPrivacyType")
    public List<Post> getByPrivacyType(@RequestParam String privacyType)
    {

        if(privacyType.equals(PrivacyType.PRIVATE))
            return postService.getPostsByPrivacyType(PrivacyType.PRIVATE);
        if(privacyType.equals(PrivacyType.PUBLIC))
            return postService.getPostsByPrivacyType(PrivacyType.PUBLIC);

        throw new EntityNotFoundException("Privacy type must be PUBLIC or PRIVATE");
    }

}
